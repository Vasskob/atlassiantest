This project is to implement Atlassian Automation Exercise. 

-Pre-requites-
* I used IntelliJ IDEA 14.1.5 
* JDK
* Trial instance of JIRA v7.0.0
* Selenium 2 (Maven)
* Firefox version 41.0.1


-Tests Description-
-- The automation test script will verify: --
* New issues can be created.
* Existing issues can be updated.
* Existing issues can be found via JIRA’s search.

-How to run test script-
-- In IntelliJ IDEA vs local instance of JIRA: --
* Change logIn data on yours in src.test.java.com.atlassian.vskoblei.CreateNewIssueIT class to provide connection to your local JIRA
* Under MAVEN project LIFECYCLE folder run VERIFY

-Test Script-
* CreateNewIssueIT - This script log in Jira, creates  new issue, verifies if issue is created, closed the browser window. Then opened a new window in browser, log in Jira, creates new issue, verifies if issue is created, searching via JIRA’s search last issue and updated it. 

-Issues-
* The Search menu is always available to click even when issue is being created. So I had to use Thread.sleep to wait for the create issue process to finish.

-Assumptions made-
The issue is searched by summary

